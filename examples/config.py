#!/usr/bin/env/python3

autostart = {
        "xrdb": ["xrdb", "~/.Xresources"],
        "feh":  ["feh", "--bg-scale", "~/.wall.jpg"]
    }

settings = {
        "binactive": "#004D40",
        "bactive": "#00BFA5",
        "bsize": 3,
        "mfocus": False,
        "igaps": 3,
        "ogaps": 10
    }


keys = {
        "mleft": {
            "control": False,
            "shift": True,
            "key": "Left",
            "type": "window_move",
            "action": "mleft"
        },
        "mright": {
            "control": False,
            "shift": True,
            "key": "Right",
            "type": "window_move",
            "action": "mright"
        },
        "mup": {
            "control": False,
            "shift": True,
            "key": "Up",
            "type": "window_move",
            "action": "mup"
        },
        "mdown": {
            "control": False,
            "shift": True,
            "key": "Down",
            "type": "window_move",
            "action": "mdown"
        },
        "terminal": {
            "control": False,
            "shift": False,
            "key": "Return",
            "type": "spawn",
            "exec": ["konsole"]
        },
        "menu": {            
            "control": False,
            "shift": False,
            "key": "d",
            "type": "spawn",
            "exec": ["dmenu_run"]
        },
        "kill": {
            "control": False,
            "shift": True,
            "key": "q",
            "type": "window_kill"
        },
        "wstack": {
            "control": False,
            "shift": True,
            "key": "Return",
            "type": "window_top_stack"
        }
}
