# Nautilor WM

This is a simple WM written in python using Xlib

## Curent feature

### Init file

If exists the wm load at startup a file called `config.py` located in `$HOME/.config/nautilorwm/config.py`
> for examples check the relative folder

The `config.py` is used to configure the wm, the keybindings and the programs that starts with the wm 

### Keybindings

For now the keybindings are located in inside the only file `__main__.py` but I will move them into a separated file

Currently there's only a few availables

- MOD + Left/Up/Down/Right: move window around
- MOD + Return: spawn urxvt terminal emulator
- MOD + D: spawn rofi menu in run mode
- MOD + Q: kill current window
           > the first window in the window list gets the focus
- MOD + LeftClick: Move window around
- MOD + RightClick: Resize window (only from resize from one corner)

# TODO:

- [DONE] Move keybindings to separated file
- [DONE] Make keybindings declaration easier
- [DONE] Window border size and colors for active/inactive windows
- [DONE] Mouse support to focus window and move the around with the modifier
- [DONE] Setting to disable focus on mouse
- [DONE] Mouse click to raise window in front
- [DONE] Allow user for keybindigs with Shift/Ctrl + Key
- Keyboard shortcut to change the focus of the window
- [MOUSE] Mouse/Keyboard support to resize window
- Everything I can think of like tiling?
