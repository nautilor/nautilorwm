#!/usr/bin/env python3

import Xlib
from Xlib import X, XK
from Xlib.display import Display
from Xlib.display import colormap 
import subprocess
from os.path import expanduser, join
import sys
import pathlib

# TODO: check if files exists
sys.path.insert(1, join(expanduser("~"), ".config/nautilorwm/"))
from config import keys, settings, autostart

class wm:
    def __init__(self):
        self.wlist   = []                                 # list of all windows
        self.dpy     = Display()                          # initialize display
        self.cm      = self.dpy.screen().default_colormap # initialize colormap
        self.cmode   = None                               # current used mode
        self.awindow = None                               # current active window
        self.rwindow = self.dpy.screen().root             # root window
        self.dpyw    = self.rwindow.get_geometry().width  # display width
        self.dpyh    = self.rwindow.get_geometry().height # display height
        self.smouse  = None
        self.wx, self.wy = None, None

        self.rwindow.change_attributes(event_mask=X.SubstructureRedirectMask) # redirect events to root
        self.hautostart()
        self.dkeys()
        self.mouse()

    def hautostart(self):
        for k in autostart:
            self.exec(autostart[k])
    
    def loop(self):
        self.ufocus()       # update focus on mouse
        self.uborders()     # update borders
        self.hevents()      # handle events
        self.tiling()

    def uborders(self):
        for w in self.wlist:
            try:
                if w != self.awindow:
                    bcolor = settings["binactive"]
                else:
                    bcolor = settings["bactive"]
                bcolor = self.cm.alloc_named_color(bcolor).pixel
                w.configure(border_width=settings["bsize"])
                w.change_attributes(None,border_pixel=bcolor)
                self.dpy.sync()
            except Exception:
                print("error")
                self.wlist.remove(w)
                self.awindow = self.awindow if w == self.awindow else None

    def ufocus(self):
        if not settings["mfocus"]:
            return
        w = self.dpy.screen().root.query_pointer().child
        if w != 0:
            self.awindow = w
            self.awindow.raise_window()

    def hevents(self):
        bevent = [3, 33, 34, 23] # bad events
        if self.dpy.pending_events():
            e = self.dpy.next_event()
            if e.type == X.MapRequest:             self.hmap(e)         # handle mapping event
            if e.type == X.KeyPress:               self.hkey(e)         # handle keypress event
            if e.type == X.MotionNotify:           self.hmouse(e)       # handle mouse event
            if e.type == X.NotifyPointer:          self.smouse = None   # delete last mouse position
            if e.type == X.NotifyNonlinearVirtual: self.cawindow(e)     # change active window
            if e.type in bevent:                   return               # ignore unwanted event

    def cawindow(self, e):
        if e.child:
            self.awindow = e.child
            if self.awindow: 
                self.awindow.raise_window()

    def gkey(self, codes, mod):
        for code in codes:
            self.rwindow.grab_key(code, mod, 1, X.GrabModeAsync, X.GrabModeAsync)

    def dkey(self, k):
        return set(c for c, i in self.dpy.keysym_to_keycodes(k))

    def mouse(self):
        MODS = X.ButtonMotionMask | X.ButtonReleaseMask | X.ButtonPressMask
        self.rwindow.grab_button(X.AnyButton, X.Mod4Mask, True, MODS, 
                X.GrabModeAsync, X.GrabModeAsync, X.NONE, X.NONE, X.NONE)

    def dkeys(self):
        self.gkeys = {}
        for key in keys:
            self.gkeys[key] = { "key": self.dkey(XK.string_to_keysym(keys[key]['key'])) }

        for k in self.gkeys:
            mod = X.Mod1Mask
            if keys[k]["shift"]: mod = mod|X.ShiftMask
            if keys[k]["control"]: mod = mod|X.ControlMask
            self.gkeys[k]['mod'] = mod
            self.gkey(self.gkeys[k]['key'], mod)


    def hmouse(self, e):
        if self.awindow and self.wlist:
            if not self.smouse: self.smouse = e
            x = e.root_x - self.smouse.root_x
            y = e.root_y - self.smouse.root_y 
            if (e.state == 264):
                self.smouse = e
                self.awindow.configure(x = self.awindow.get_geometry().x + x,
                                       y = self.awindow.get_geometry().y + y)
            if (e.state == 1032):
                w = 2 if x > self.smouse.root_x else -2
                h = 2 if y > self.smouse.root_y  else -2
                if x == self.wx: w = 0
                if y == self.wy: h = 0
                self.wx, self.wy = x, y
                w = self.awindow.get_geometry().width + w
                h = self.awindow.get_geometry().height + h
                self.awindow.configure(width=w, height=h)

    def mwindow(self, d):
        try:
            w = self.awindow.get_geometry().x
            h = self.awindow.get_geometry().y
            if   d == "mleft":    self.awindow.configure(x=w-5)
            elif d == "mright":   self.awindow.configure(x=w+5)
            elif d == "mup":      self.awindow.configure(y=h-5)
            elif d == "mdown":    self.awindow.configure(y=h+5)
            else:                 print("Invalid movement direction!")
        except Exception:         print("No focused window!")

    def hmap(self, e):
        self.wlist.insert(0, e.window)
        self.awindow = e.window
        e.window.map()

    def exec(self, c):
        try:
            c = [expanduser(i) for i in c]
            subprocess.Popen(c)
        except BaseException as e:
            print("error")

    def kwin(self):
        if self.awindow:
            self.awindow.destroy()
            self.wlist.remove(self.awindow)
            if len(self.wlist) > 0:
                self.awindow = self.wlist[-1] if self.wlist else None
                self.awindow.raise_window()

    def tiling(self):
        if not self.wlist:
            return
        ogaps = settings['ogaps']
        igaps = settings['igaps']
        twindow_w = self.dpyw - 5 - ogaps * 2
        twindow_h = self.dpyh - 5 - ogaps * 2
        if len(self.wlist) == 1:
            self.awindow.configure(x= 0 + ogaps, y = 0 + ogaps,
                    width=twindow_w, height=twindow_h)
        else:
            twindow_w = int(twindow_w/2)
            self.awindow.configure(x= 0 + ogaps, y = 0 + ogaps,
                    width=twindow_w - igaps*4, height=twindow_h)
            cyi = 0
            wn = len(self.wlist[1:])
            wss = self.wlist[1:]
            h = int(twindow_h/wn)
            for w in wss:
                y = cyi + ogaps
                if w == wss[0]: 
                    w.configure(x = twindow_w + igaps*4, y = y,
                        width=twindow_w, height=h) 
                else:
                    w.configure(x = twindow_w + igaps*4, y = y,
                        width=twindow_w, height=h-ogaps) 
                cyi += h

    def hkey(self, e):
        for k in keys:
            key = keys[k]
            if e.detail in self.gkeys[k]['key'] and \
                    e.state == self.gkeys[k]['mod']:
                if key['type'] == "window_move":
                    self.mwindow(key['action'])
                elif key['type'] == 'window_top_stack':
                    if self.awindow:
                        self.wlist.remove(self.awindow)
                        self.wlist.insert(0, self.awindow)
                elif key['type'] == "spawn":
                    self.exec(key['exec'])
                elif key['type'] == "window_kill":
                    self.kwin()

    def kwm(self):
        self.dpy.close()

if __name__ == "__main__":
    manager = wm()
    print("running...")
    while True:
        manager.loop()
